package assignment4;

import java.util.Random;
/**
 * 
 * @author Andrew Wilkerson
 * CST-135
 * This is my own original work
 *
 */

public class Game {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Random rand = new Random();
		int health1 = rand.nextInt(1000) + 1;
		int health2 = rand.nextInt(1000) + 1;
		
		System.out.println("Creating our Super Heros!");
		Superman superman = new Superman(health1);
		Batman batman = new Batman(health2);
		System.out.println("Super heros created!");
		
		//Run the game until someone is dead
		System.out.println("Runing the game....");
		while(!superman.isDead() && !batman.isDead()) {
			
			//Attack each other
			//If rand1 is 5 then the Super hero uses special attack otherwise they use normal attack
			Random rand1 = new Random();
			if(rand1.nextInt(10) + 1 == 5) {
				superman.xRayVision(batman);
			}
			else {
				superman.attack(batman);
			}
			
			if(rand1.nextInt(10) + 1 == 5) {
				batman.planZ(superman);
			}
			else {
				batman.attack(superman);
			}
			
			
			//See if anyone is alive
			if(superman.isDead()) {
				System.out.println("Batman defeated Superman.");
			}
			if(batman.isDead()) {
				System.out.println("Superman defeated Batman.");
			}
		}

	}

}
