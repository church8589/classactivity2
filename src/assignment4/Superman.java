package assignment4;

import java.util.Random;
/**
 * 
 * @author Andrew Wilkerson
 * CST-135
 * This is my own original work
 *
 */
public class Superman extends SuperHero {
	
	public Superman(int health) {
		super("Superman", health);
	}
	
	/**
	 * Special attack for Superman
	 * @param opponent Other Superhero Supermna is fighting
	 */
	public void xRayVision(SuperHero opponent) {
		//superman finds a weak point to exploit
		System.out.println("Superman finds a weak point!");
		Random rand = new Random();
		int damage = rand.nextInt(25) + 1;
		
		opponent.determineHealth(damage);
		System.out.printf("%s has damage of %d and health of %d\n", opponent.name, damage, opponent.health);
	}

}
