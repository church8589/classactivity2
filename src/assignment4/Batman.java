package assignment4;

import java.util.Random;
/**
 * 
 * @author Andrew Wilkerson
 * CST-135
 * This is my own original work
 *
 */
public class Batman extends SuperHero {

	public Batman(int health) {
		super("Batman", health);
	}
	
	/**
	 * Special attack for Batman
	 * @param opponent Other Superhero batman is fighting
	 */
	public void planZ(SuperHero opponent) {
		//batman has a plan for everyone
		System.out.println("Batman always has a plan!");
		Random rand = new Random();
		int damage = rand.nextInt(25) + 1;
		
		opponent.determineHealth(damage);
		System.out.printf("%s has damage of %d and health of %d\n", opponent.name, damage, opponent.health);
	}

}
