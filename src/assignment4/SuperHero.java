package assignment4;

import java.util.Random;
/**
 * 
 * @author Andrew Wilkerson
 * CST-135
 * This is my own original work
 *
 */
public class SuperHero {
	String name;
	int health;
	boolean isDead;
	


	public SuperHero(String name, int health) {
		this.name = name;
		this.health = health;
	}
	
	/**
	 * Attack the oppenent and determine how much damage is done and remaing health
	 * @param opponent Other superhero that is being fought
	 */
	public void attack(SuperHero opponent) {
		Random rand = new Random();
		int damage = rand.nextInt(10) + 1;
		
		opponent.determineHealth(damage);
		System.out.printf("%s has damage of %d and health of %d\n", opponent.name, damage, opponent.health);
	}
	
	/**
	 * Determine whether the damage killed Superhero or not
	 * @param damage how much to reduce health by
	 */
	public void determineHealth(int damage) {
		
		if(this.health - damage <= 0) {
			this.health = 0;
			this.isDead = true;
		}
		else {
			this.health = this.health - damage;
		}
	}
	
	/**
	 * Determine if isDead() is true or false
	 * @return
	 */
	public boolean isDead() {
		return this.isDead;
	}
	
}